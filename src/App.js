import './styles/App.css';
import './styles/tailwind.css';
import React, { Component } from 'react'
import SearchBar from './components/SearchBar';
import EmojiItem from './components/EmojiItem';

export default class App extends Component {
  constructor(props) {
    super(props)
    this.searchInputRef = React.createRef();
    this.state = {
      searchItem: '',
      emojis: null
    }
  }
  componentDidMount(){
    this.searchInputRef.current.focus()
  }
  // componentDidUpdate(){
    
  // }
  searchInputHandler = event => {
    const keyCode = event.key;
    if (keyCode === 'Enter') {
      const key = `39ccca5fca44aae6085cbd74dc8f8aa335adefcc`;
      fetch(`https://emoji-api.com/emojis?search=${event.target.value}&access_key=${key}`)
      .then(response=>response.json())
      .then(result=>{
        this.setState({
          searchItem: event.target.value,
          emojis: result
        }, ()=>console.log(this.state))
      })
    }
  }
  render() {
    return (
      <>
        <div className="text-center my-8">
          <h1 className="text-4xl font-bold uppercase text-gray-700">emoji search</h1>
        </div>
        <SearchBar ref={this.searchInputRef} searchInputHandler={this.searchInputHandler}/>
        {
          this.state.searchItem
          ? this.state.emojis
            ? <div style={{margin: "auto", padding: '10%'}}>
                <ul>
                  {
                    this.state.emojis.map(emoji=>
                      <EmojiItem emoji={emoji}/>
                    )
                  }
                </ul>
              </div>
            : <h1 className="text-center">Emoji Not Found</h1>
          : null
        }
      </>
    )
  }
}