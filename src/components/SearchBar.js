import React from 'react';

const SearchBar = React.forwardRef((props, ref)=>{
    return (
        <div className="p-4">
            <input type="search" className="block p-2 rounded-full outline-none border-2 shadow-sm focus:shadow-lg text-center w-96 text-gray-500" style={{margin: 'auto'}} ref={ref} placeholder="search emoji" onKeyPress={props.searchInputHandler}/>
        </div>
    )
})
export default SearchBar
