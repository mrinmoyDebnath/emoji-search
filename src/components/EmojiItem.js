import React from 'react'

function EmojiItem(props) {
    return (
        <li className="text-6xl w-64 p-8 text-center float-left">
            {props.emoji.character}            
        </li>
    )
}

export default EmojiItem
